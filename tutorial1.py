######################################################################################################
# @file        tutorial1.py
# @brief
##########################################################
#
# Resources : https://www.youtube.com/watch?v=bMHK6NDVlCM&list=PLzMcBGfZo4-kSJVMyYeOQ8CXJ3z1k7gHn
#             https://www.youtube.com/watch?v=LDnRhg9OywU
#
# Modifications:   12/08/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python3
import kivy
from kivy.app import App
from kivy.uix.label import Label


class MyApp(App):
    def build(self):
        return Label(text="Tech With Ron")


if __name__ == "__main__":
    MyApp().run()