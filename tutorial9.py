######################################################################################################
# @file        tutorial9.py
# @brief
##########################################################
#
# Resources : https://www.youtube.com/watch?v=xaYn4XdieCs&list=PLzMcBGfZo4-kSJVMyYeOQ8CXJ3z1k7gHn&index=9
#
# Modifications:   12/08/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python3
import kivy
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen


class MainWindow(Screen):
    pass


class SecondWindow(Screen):
    pass


class WindowManager(ScreenManager):
    pass


kv = Builder.load_file("tutorial9.kv")


class MyMainApp(App):
    def build(self):
        return kv


if __name__ == "__main__":
    MyMainApp().run()
