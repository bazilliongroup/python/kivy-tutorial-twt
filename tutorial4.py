######################################################################################################
# @file        tutorial4.py
# @brief
##########################################################
#
# Resources :
#
# Modifications:   12/08/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python3
import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.widget import Widget


class MyGrid(Widget):
    pass


class Tutorial4(App):
    def build(self):
        return MyGrid()


if __name__ == "__main__":
    Tutorial4().run()
