######################################################################################################
# @file        tutorial7.py
# @brief
##########################################################
#
# Resources : https://www.youtube.com/watch?v=yg7n8hP6k1w&list=PLzMcBGfZo4-kSJVMyYeOQ8CXJ3z1k7gHn&index=7
#
# Modifications:   12/08/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python3
import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty


class Touch(Widget):
    btn = ObjectProperty(None)

    def on_touch_down(self, touch):
        print("Mouse Down", touch)
        self.btn.opacity = 0.5

    def on_touch_move(self, touch):
        print("Mouse Move", touch)

    def on_touch_up(self, touch):
        print("Mouse Up", touch)
        self.btn.opacity = 1

class tutorial7(App):
    def build(self):
        return Touch()


if __name__ == "__main__":
    tutorial7().run()
