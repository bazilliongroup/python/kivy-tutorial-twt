######################################################################################################
# @file        tutorial6.py
# @brief
##########################################################
#
# Resources :
#
# Modifications:   12/08/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python3
import kivy
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout


class Tutorial6(App):
    def build(self):
        return FloatLayout()


if __name__ == "__main__":
    Tutorial6().run()