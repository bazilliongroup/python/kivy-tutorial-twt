######################################################################################################
# @file        tutorial3.py
# @brief
##########################################################
#
# Resources : https://www.youtube.com/watch?v=QUHnJrFouv8&list=PLzMcBGfZo4-kSJVMyYeOQ8CXJ3z1k7gHn&index=2
#
#         ###########################################
#         #  First Name:    -                       #
#         #                 -                       #
#         #-----------------------------------------#
#         #  Last Name:     -                       #
#         #                 -                       #
#         #-----------------------------------------#
#         #  Email:         -                       #
#         #                 -                       #
#         ###########################################
#         #                                         #
#         #              SUBMIT                     #
#         #                                         #
#         ###########################################
#
# Modifications:   12/08/2020   R. Bazillion    First Draft
#
######################################################################################################
# !/usr/bin/python3
import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button


class MyGrid(GridLayout):
    def __init__(self, **kwargs):
        super(MyGrid, self).__init__(**kwargs)
        self.cols = 1  # Set columns for main layout

        # Add in the text
        self.name = TextInput(multiline=False)
        self.lastName = TextInput(multiline=False)
        self.email = TextInput(multiline=False)

        # ALL OF THESE ARE APART OF THE (INTERIOR)NEW LAYOUT
        self.inside = GridLayout()  # Create a new grid layout
        self.inside.cols = 2  # set columns for the new grid layout
        self.inside.add_widget(Label(text="First Name: "))
        self.inside.add_widget(self.name)
        self.inside.add_widget(Label(text="Last Name: "))
        self.inside.add_widget(self.lastName)
        self.inside.add_widget(Label(text="Email: "))
        self.inside.add_widget(self.email)
        # -------------------------------------------------

        # Add the inside widget into the main Widget
        self.add_widget(self.inside)

        # Submit button
        self.submit = Button(text="Submit", font_size=40)
        self.submit.bind(on_press=self.pressed)

        # add button to widget
        self.add_widget(self.submit)  # Add the button to the main layout

    def pressed(self, instance):
        name = self.name.text
        last = self.lastName.text
        email = self.email.text

        # print the info
        print("Name:", name, "Last Name:", last, "Email:", email)

        # clear the info after the button press.
        self.name.text = ""
        self.lastName.text = ""
        self.email.text = ""


class MyApp(App):
    def build(self):
        return MyGrid()


if __name__ == "__main__":
    MyApp().run()
